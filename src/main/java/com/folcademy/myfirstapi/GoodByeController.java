package com.folcademy.myfirstapi;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodByeController {
    @PostMapping("/Goodbye")
        public String goodByeWorld()
        {
            return "Good Bye!";
        }
}
